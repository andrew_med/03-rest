package ru.volnenko.web.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.volnenko.se.entity.Project;
import ru.volnenko.se.repository.ProjectRepository;

@Controller
@RequestMapping("/project")
public class ProjectController {
	
	@Autowired
	private ProjectRepository projectRepository;
	
    @GetMapping("/list")
    public String list(Model model) {
    	model.addAttribute("projects", projectRepository.findAll());
        return "project/list";
    }
    
    @GetMapping("/remove/{id}")
    public String remove(@PathVariable String id) {
    	projectRepository.deleteById(id);
        return "redirect:/project/list";
    }

    @GetMapping("/edit")
    public String edit(@RequestParam(required = false) String id, Model model) {
    	Project project = (id == null) ? null : projectRepository.findById(id).orElse(null);
    	if(project == null) project = new Project();
    	model.addAttribute("project", project);
        return "project/edit";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("project") Project project) {
    	projectRepository.save(project);
        return "redirect:/project/list";
    }
}
