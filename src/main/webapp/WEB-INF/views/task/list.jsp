<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>

<head>
<style>
table, th, td {
  border: 1px solid gray;
  border-collapse: collapse;
}
th, td {
  padding: 10px;
}
</style>
</head>

<body>

<%@ include file = "../header.jsp" %>

<h1>Task Management</h1>

<table>
  <tr>
    <th>ID</th>
    <th>Name</th>
  </tr>
  <c:forEach items="${tasks}" var="task">
  <tr>
    <td>${task.id}</td>
    <td>${task.name}</td>
    <td><a href="edit?id=${task.id}">Edit</a></td>
    <td><a href="remove/${task.id}">Remove</a></td>
  </tr>
  </c:forEach>
</table>

<p><a href="edit">New Task</a></p>
<p><a href="list">Refresh</a></p>

</body>
</html>
